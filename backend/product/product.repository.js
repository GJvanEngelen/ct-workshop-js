const Product = require("./product");

class ProductRepository {
  constructor() {
    this.products = new Map([
      [
        "09",
        new Product("09", "Frontend Test Automation",
         "Selenium Course", "v1"),
      ],
      [
        "10",
        new Product("10", "Frontend Test Automation",
         "Puppeteer Course", "v1"),
      ],
      [
        "11",
        new Product("11", "Testing principles",
         "Continuous Testing", "v2"),
      ],
    ]);
  }

  async fetchAll() {
    return [...this.products.values()];
  }

  async getById(id) {
    return this.products.get(id);
  }
}

module.exports = ProductRepository;
