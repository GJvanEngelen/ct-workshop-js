const psList = require('ps-list');
module.exports = async function killSubprocesses(pid = process.pid, signal = 'SIGINT') {
  const list = await psList();
  // find leaf process and kill
  function findChildren(pid) {
    const children = list.filter(item => ['node', 'npm'].includes(item.name) && item.ppid === pid);
    for (const child of children) {
      const subChildren = findChildren(child.pid);
      children.push(...subChildren);
    }

    return children.reverse();
  }

  for (const child of findChildren(pid)) {
    try {
      process.kill(child.pid, signal);
    } catch (err) {}
  }
}
